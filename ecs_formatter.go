package logger

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
)

type fieldKey string

// FieldMap contains all the fields that go into log entries
type FieldMap map[fieldKey]string

const (
	// FieldKeyMsg is the key for message
	FieldKeyMsg = "message"
	// FieldKeyLevel is the key for level
	FieldKeyLevel = "level"
	// FieldKeyTime is the key for time
	FieldKeyTime = "time"
	// FieldKeyData is the key for data
	FieldKeyData = "data"
	// DefaultTimestampFormat is the default formtat for presenting time
	DefaultTimestampFormat = time.RFC3339
)

func (f FieldMap) resolve(key fieldKey) string {
	if k, ok := f[key]; ok {
		return k
	}

	return string(key)
}

// ECSFormatter formats logs in ecs-logger compatible format
// with fields inside data
type ECSFormatter struct {
	// TimestampFormat sets the format used for marshaling timestamps.
	TimestampFormat string

	// DisableTimestamp allows disabling automatic timestamps in output
	DisableTimestamp bool

	// FieldMap allows users to customize the names of keys for various fields.
	// As an example:
	// formatter := &JSONFormatter{
	//   	FieldMap: FieldMap{
	// 		 FieldKeyTime: "@timestamp",
	// 		 FieldKeyLevel: "@level",
	// 		 FieldKeyMsg: "@message",
	//    },
	// }
	FieldMap FieldMap
}

// NewFormatter creates a new instance of ECS Formatter.
func NewFormatter() *ECSFormatter {
	return &ECSFormatter{}
}

// Format formats an entry according to logrus
func (f *ECSFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	data := make(logrus.Fields, 4)

	timestampFormat := f.TimestampFormat
	if timestampFormat == "" {
		timestampFormat = DefaultTimestampFormat
	}

	if !f.DisableTimestamp {
		data[f.FieldMap.resolve(FieldKeyTime)] = entry.Time.Format(timestampFormat)
	}
	data[f.FieldMap.resolve(FieldKeyMsg)] = entry.Message
	data[f.FieldMap.resolve(FieldKeyLevel)] = entry.Level.String()

	if len(entry.Data) > 0 {
		dataKey := make(logrus.Fields, len(entry.Data))
		for k, v := range entry.Data {
			switch v := v.(type) {
			case error:
				// Otherwise errors are ignored by `encoding/json`
				// https://github.com/sirupsen/logrus/issues/137
				dataKey[k] = v.Error()
			default:
				dataKey[k] = v
			}
		}
		data[f.FieldMap.resolve(FieldKeyData)] = dataKey
	}

	serialized, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal fields to JSON, %v", err)
	}
	return append(serialized, '\n'), nil
}
