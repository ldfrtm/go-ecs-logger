package logger

import (
	"net"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
)

// loggingHandler is the http.Handler implementation for LoggingHandlerTo and its
// friends
type loggingHandler struct {
	log     logrus.FieldLogger
	handler http.Handler
}

func (h loggingHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	t := time.Now()
	logger := makeLogger(w)
	url := *req.URL
	h.handler.ServeHTTP(logger, req)
	writeLog(h.log, req, url, t, logger.Status(), logger.Size())
}

func makeLogger(w http.ResponseWriter) loggingResponseWriter {
	var logger loggingResponseWriter = &responseLogger{w: w, status: http.StatusOK}
	return logger
}

func writeLog(log logrus.FieldLogger, req *http.Request, url url.URL, ts time.Time, status, size int) {
	fields := buildFields(req, url, ts, status, size)
	log.WithFields(fields).Info("Handle HTTP request")
}

type loggingResponseWriter interface {
	http.ResponseWriter
	http.Flusher
	Status() int
	Size() int
}

// responseLogger is wrapper of http.ResponseWriter that keeps track of its HTTP
// status code and body size
type responseLogger struct {
	w      http.ResponseWriter
	status int
	size   int
}

func (l *responseLogger) Header() http.Header {
	return l.w.Header()
}

func (l *responseLogger) Write(b []byte) (int, error) {
	size, err := l.w.Write(b)
	l.size += size
	return size, err
}

func (l *responseLogger) WriteHeader(s int) {
	l.w.WriteHeader(s)
	l.status = s
}

func (l *responseLogger) Status() int {
	return l.status
}

func (l *responseLogger) Size() int {
	return l.size
}

func (l *responseLogger) Flush() {
	f, ok := l.w.(http.Flusher)
	if ok {
		f.Flush()
	}
}

func buildFields(req *http.Request, url url.URL, ts time.Time, status int, size int) map[string]interface{} {
	username := "-"
	if url.User != nil {
		if name := url.User.Username(); name != "" {
			username = name
		}
	}

	host, _, err := net.SplitHostPort(req.RemoteAddr)

	if err != nil {
		host = req.RemoteAddr
	}

	uri := req.RequestURI

	// Requests using the CONNECT method over HTTP/2.0 must use
	// the authority field (aka r.Host) to identify the target.
	// Refer: https://httpwg.github.io/specs/rfc7540.html#CONNECT
	if req.ProtoMajor == 2 && req.Method == "CONNECT" {
		uri = req.Host
	}
	if uri == "" {
		uri = url.RequestURI()
	}

	return logrus.Fields{
		"host":     host,
		"username": username,
		"ts":       ts.Format("02/Jan/2006:15:04:05 -0700"),
		"method":   req.Method,
		"uri":      uri,
		"protocol": req.Proto,
		"status":   strconv.Itoa(status),
		"size":     strconv.Itoa(size),
	}
}

// LoggingHandler return a http.Handler that wraps h and logs requests to out in
// ECS Format
func LoggingHandler(l logrus.FieldLogger, h http.Handler) http.Handler {
	return loggingHandler{l, h}
}

// Handler uses Logging handler with a new handler
func Handler(h http.Handler) http.Handler {
	l := logrus.New()
	l.Formatter = NewFormatter()
	return LoggingHandler(l, h)
}
