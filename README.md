go-ecs-logger
=============

[Logrus](https://github.com/sirupsen/logrus) formatter that generates logs for [ecs-logs](https://github.com/segmentio/ecs-logs) forwarder.

This package contains a formatter and a `http.Handler` that will log requests.

## Usage

Set the formatter for the default logger by doing

```go
import (
	"bitbucket.org/ldfrtm/go-ecs-logger"
	"github.com/sirupsen/logrus"
)

func init() {
	logrus.SetFormatter(logger.NewFormatter())
}
```

## Output

Follows the format of [ecs-logs](https://github.com/segmentio/ecs-logs) and adds every structured field inside `"data" : {}`.