package logger

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/sirupsen/logrus"
	ht "github.com/sirupsen/logrus/hooks/test"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLogHandler(t *testing.T) {
	Convey("When handler is succesfull", t, func() {

		h := func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "response")
		}
		Convey("Then a log entry is created", withHTTP(h, func(hook *ht.Hook) {
			So(hook.AllEntries(), ShouldHaveLength, 1)
			e := hook.LastEntry()
			So(e.Level, ShouldEqual, logrus.InfoLevel)
			So(e.Message, ShouldEqual, "Handle HTTP request")
			So(e.Data["method"], ShouldEqual, "GET")
			So(e.Data["uri"], ShouldEqual, "/")
			So(e.Data["protocol"], ShouldEqual, "HTTP/1.1")
			So(e.Data["status"], ShouldEqual, "200")
			So(e.Data["size"], ShouldEqual, "8")
		}))
	})
}

func withHTTP(handler http.HandlerFunc, testFunction func(*ht.Hook)) func() {
	return func() {
		log, hook := ht.NewNullLogger()

		lh := LoggingHandler(log, handler)

		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/", nil)

		lh.ServeHTTP(w, r)

		testFunction(hook)
	}
}
