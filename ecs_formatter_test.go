package logger

import (
	"encoding/json"
	"errors"
	"testing"

	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
)

type LE struct {
	Data map[string]interface{}
}

func TestECSFormatter(t *testing.T) {

	Convey("Errors are not lost", t, func() {
		formatter := &ECSFormatter{}
		b, err := formatter.Format(logrus.WithField("error", errors.New("wild walrus")))
		So(err, ShouldBeNil)

		entry := LE{}
		err = json.Unmarshal(b, &entry)
		So(err, ShouldBeNil)
		So(entry.Data["error"], ShouldEqual, "wild walrus")
	})

	Convey("Error are not lost on field not named error", t, func() {
		formatter := &ECSFormatter{}
		b, err := formatter.Format(logrus.WithField("omg", errors.New("wild walrus")))
		So(err, ShouldBeNil)

		entry := LE{}
		err = json.Unmarshal(b, &entry)
		So(err, ShouldBeNil)
		So(entry.Data["omg"], ShouldEqual, "wild walrus")
	})

	Convey("JSON entry ends with new line", t, func() {
		formatter := &ECSFormatter{}
		b, err := formatter.Format(logrus.WithField("level", "something"))
		So(err, ShouldBeNil)
		So(b[len(b)-1], ShouldEqual, '\n')
	})

	Convey("JSON Message Key", t, func() {
		formatter := &ECSFormatter{
			FieldMap: FieldMap{
				FieldKeyMsg: "message",
			},
		}

		b, err := formatter.Format(&logrus.Entry{Message: "oh hai"})
		So(err, ShouldBeNil)

		s := string(b)
		So(s, ShouldContainSubstring, "message")
		So(s, ShouldContainSubstring, "oh hai")
	})

	Convey("JSON Level key", t, func() {
		formatter := &ECSFormatter{
			FieldMap: FieldMap{
				FieldKeyLevel: "somelevel",
			},
		}

		b, err := formatter.Format(logrus.WithField("level", "something"))
		So(err, ShouldBeNil)

		s := string(b)
		So(s, ShouldContainSubstring, "somelevel")
	})

	Convey("JSON Time Key", t, func() {
		formatter := &ECSFormatter{
			FieldMap: FieldMap{
				FieldKeyTime: "timeywimey",
			},
		}

		b, err := formatter.Format(logrus.WithField("level", "something"))
		So(err, ShouldBeNil)
		s := string(b)
		So(s, ShouldContainSubstring, "timeywimey")
	})

	Convey("JSON Disable timestamp", t, func() {
		formatter := &ECSFormatter{
			DisableTimestamp: true,
		}

		b, err := formatter.Format(logrus.WithField("level", "something"))
		So(err, ShouldBeNil)
		s := string(b)
		So(s, ShouldNotContainSubstring, FieldKeyTime)
	})
}
